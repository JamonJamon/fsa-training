<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AssessmentsController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('/supervisor', 'AssessmentsController');

Route::get('/supervisor', 'AssessmentsController@index');
Route::get('/supervisor/async/{assessment}', 'AssessmentsController@asyncGet');
Route::get('/supervisor/create', 'AssessmentsController@create');
Route::get('/supervisor/edit', 'AssessmentsController@edit');
Route::get('/supervisor/edit/{assessment}', 'AssessmentsController@edit');
Route::get('/supervisor/show/{assessment}', 'AssessmentsController@show');
Route::post('/supervisor', 'AssessmentsController@store');
Route::patch('/supervisor/{assessment}', 'AssessmentsController@update');
Route::patch('/supervisor/async/{assessment}', 'AssessmentsController@asyncUpdate');


Route::post('/area', 'AreaController@store');

Route::post('/question', 'QuestionController@store');