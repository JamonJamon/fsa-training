const metas = document.getElementsByTagName('meta');
//var csrf = document.getElementsByName('csrf-token');

for (let i = 0; i < metas.length; i++) {
  if (metas[i].getAttribute('name') === 'csrf-token') {
    var csrfToken = metas[i].getAttribute('content');
  }
}

id ='';
areas = '';
questions = '';

document.addEventListener("DOMContentLoaded", show());

var container = document.getElementById('assessment-container');

let titleDiv = document.createElement('div');
titleDiv.setAttribute('classs', 'container');

let job = document.createElement('input');
job.setAttribute('id', 'job');
job.setAttribute('placeholder', 'Assign Job');

titleDiv.appendChild(job);



let inpTableDiv = document.createElement('div');
let table = document.createElement('table');
table.setAttribute('id', 'table');
table.setAttribute('class', 'table');

let caption = document.createElement('caption');

caption.innerHTML='<input id="assignArea" type="text" placeholder="Assign Area">';

var row = table.insertRow();
var cell0 = row.insertCell();
var cell1 = row.insertCell();
row.setAttribute('class', 'thead-dark');

cell0.innerHTML= '<input id="assignQuestion" type="text" placeholder="Assign question">';
cell1.innerHTML= '<button type="select" onclick="assign();">Save Assessment</button>';

inpTableDiv.appendChild(table);

container.appendChild(titleDiv);
container.appendChild(caption);
container.appendChild(inpTableDiv);
var inputs;
    
    
function assign(){
  var job = document.getElementById('job').value;
  var questionValue = document.getElementById('assignQuestion').value;
  var areaValue = document.getElementById('assignArea').value;

  assessArray = [];
  
  for(i=0; i<1; i++){
    questionArray = [];
  areaArray = {"area": areaValue, "question": questionArray};
  questionArray.push(questionValue);
  assessArray.push(areaArray);
}
areaTable = JSON.stringify(assessArray);
console.log(areaTable);
  var processedData = new FormData();
  processedData.append("json", areaTable);
  processedData.append("job", job);
//  processedData.append("supervisor_id", 1);

fetch("/supervisor", {
  headers: {
    // "Content-Type": "application/json",
    // "Accept": "application/json, text-plain, */*",
    // "X-Requested-With": "XMLHttpRequest",
    "X-CSRF-Token": csrfToken
  },
  method: "POST",
  credentials: "same-origin",
  body: processedData
  });
  
}
data = [];
function show(){
  fetch(APP_URL+"/supervisor/async/"+id)
    .then(function(response) {
  
      return response.json();
    })
    .then(function(data){
    
      tablesFromDb(data);
    });
  }

  function tablesFromDb(data){
    for (var i = 0;  i < data.length; i++) {
    let div = document.createElement('div');
    let title = document.createElement('caption');
    let table = document.createElement('table');
    // let removeTbl = document.createElement('button');
    div.setAttribute('id', 'div'+i); 
    table.setAttribute('id', 'tbl');
    table.setAttribute('class', 'table');
    // removeTbl.setAttribute('id', i);
    // removeTbl.setAttribute('class', 'removetable');
    // removeTbl.setAttribute('onclick', 'deleteTable(this.id);');
    // removeTbl.innerText = "Delete Device";
    var row = table.insertRow(0);
    row.setAttribute('class', 'header'); 
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
   
    cell1.innerHTML = 'Question';
    //cell2.innerHTML = '';
  
    
    for (var j = 0; j < data[i].question.length; j++) {
      var dataRow = table.insertRow();
      var dataCell0 = dataRow.insertCell();
      var dataCell1 = dataRow.insertCell();
   
   
      dataCell0.innerHTML = '<input class="cell" type="text" id="inp" value="'+data[i].question[j]+'" />';
      dataCell0.setAttribute('class', 'data');
      dataCell1.innerHTML = '<button class="button">Remove</button>';
      
    }
    
    title.innerHTML = '<input type="text" class="name" id="name" value="'+data[i].area+'" />';
    
    div.appendChild(title);
    div.appendChild(table);
   // div.appendChild(removeTbl);
    container.appendChild(div);
    }
  }
