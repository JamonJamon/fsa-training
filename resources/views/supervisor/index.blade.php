@extends('layouts.app')

@section('content')

<div class="container">
    <div class="mx-auto text-center" style="width: 50%;" >
        <h1>Select Assessment</h1>
    </div>

    <ul class="list-group mx-auto" style="width: 50%;">
        @foreach ($assess as $item)
    <a href="/supervisor/show/{{ $item->id }}">
            <li class="list-group-item list-group-item-light font-weight-bold text-center">
               
            {{ $item->job }}
            </li>
        </a>
        @endforeach
    </ul>
</div>
@endsection