@extends('layouts.app')

@push('js')
    
    {{-- <script src="{{ asset('js/autocomplete.js') }}" defer></script> --}}
@endpush

@section('content')
<div class="container mx-auto text-center my-5">
    <p>Input the job, area and assessment question you need to begin an assessment</p>
</div>

<div class="container ">
    <form class="form-group" id="editForm" method="POST" action="/supervisor">
        @csrf
            <div class="mt-2 mb-4">
                <input class="form-control mx-auto text-center" style="width: 250px;" id="job" type="text" name="job" placeholder="Job">
            </div>
                <div>
                    <input class="form-control mx-auto text-center my-2" style="width: 550px;" type="text" name="area[]" placeholder="Add an Area">
                    <input class="form-control mx-auto text-center my-2" style="width: 550px;" type="text" name="question[]" placeholder="Add a Question">
                </div>
                <div class="d-flex justify-content-center mt-5">
                    <button class="btn btn-primary btn-large" type="sumbit">Save this assessment</button>
                </div>
            </div>
    </form>
</div>


@endsection