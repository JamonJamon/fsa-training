@extends('layouts.app')

@section('content')


<div class="container">
         
    <div class="mx-auto text-center" style="width: 250px;"> 
        <h1 class="font-weight-bold">{{ $assessment->job }}</h1>  
    </div>
        
    @for ($i = 0; $i < count($assess); $i++)    
        <div class="mx-auto" style="width: 600px;">
            <div class="mt-4 mb-2">
                <h4 class="mx-auto text-center font-weight-bold" style="width: 250px; background-color: white;">{{ $assess[$i]['area'] }}</h4>
            </div>
            @for ($j = 0; $j < count($assess[$i]['question']); $j++)
            <div class="d-flex justify-content-between mx-auto my-2" style="background-color: white; width: 600px;">
                <p class="text-lg-left">{{ $assess[$i]['question'][$j] }}</p>
                <div class="d-flex justify-content-end">
                    <button class="btn btn-success btn-group-small mx-1" style="width: 90px;">Good</button>
                    <button class="btn btn-warning btn-group-small mx-1" style="width: 90px;">Pass</button>
                    <button class="btn btn-danger btn-group-small mx-1" style="width: 90px;">Bad</button>
                </div>
            </div>
        </div>
            @endfor
    @endfor

    <div class="d-flex justify-content-center mt-5">
        <a href="/supervisor/edit/{{ $assessment->id }}">
            <button class="btn btn-primary btn-large" type="sumbit">Edit this assessment</button>
        </a>
    </div>

</div>
@endsection