@extends('layouts.app')

@push('js')
    <script src="{{ asset('js/edit.js') }}" defer></script> 
    {{-- <script src="{{ asset('js/autocomplete.js') }}" defer></script> --}}
@endpush

@section('content')


    {{-- <div class="container">
        <div class="page-header">
            <h1>Build Assessment</h1>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <form method="POST" action="/question">
          {{ csrf_field() }}
    
            <div class="form-group">
                <input type="text" class="form-control" name="question" placeholder="Assessment Question">
            </div>
            <div class="form-group">
                    <button type="submit" class="btn btn-dark">Save question</button>
            </div>
        </form>
        <form method="POST" action="/area">
            {{ csrf_field() }}
        <div class="form-group">
                <input type="text" class="form-control" name="area" placeholder="Work Area">
            </div>
            <div class="form-group">
                    <button type="submit" class="btn btn-dark">Save Work Area</button>
            </div>
        </form>
    </div> --}}

<div class="container">
    <form class="form-group" id="editForm" method="POST" action="/supervisor/{{ $assessment->id }}">
        {{ method_field('PATCH') }}
        @csrf
        
        <h2>
            <div>
              
                <input class="form-control mx-auto" style="width: 250px;" id="job" type="text" name="job" value="{{ $assessment->job }}">
               
            </div>
            
        @for ($i = 0; $i < count($assess); $i++)

                   
            <div class="mt-4 mb-2">
                <input class="form-control mx-auto" style="width: 250px;" type="text" id="area".$i name="area[]" value="{{ $assess[$i]['area'] }}">
            </div>
                  
                {{-- <ul class="list-group cProductsList"> --}}
                @for ($j = 0; $j < count($assess[$i]['question']); $j++)
                        <div class="d-flex justify-content-between mx-auto" style="width: 550px;">
                        {{-- <li class="list-group-item d-flex justify-content-between"> --}}
                            <input class="form-control" data-toggle="button" aria-pressed="false" autocomplete="off" type="text" id="{{ $i.$j }}" name="{{ $i }}[]" value="{{ $assess[$i]['question'][$j] }}" placeholder="{{ $assess[$i]['question'][$j] }}">
                            <button class="btn btn-danger" id="{{ $i.$j }}" onclick="deleteQuestion(this);">Remove</button>
                        {{-- </li> --}}
                        </div>
                @endfor
                {{-- </ul> --}}
                        <input class="form-control mx-auto" style="width: 550px;"  type="text" name="{{ $i }}[]"  placeholder="Add a question"><br>
                        
        @endfor
                <div>
                    
                        <input class="form-control mx-auto" style="width: 550px;" type="text" name="addArea" placeholder="Add an Area">
                    
                    
                        <input type="hidden" name="addQuestion[]" value="Add a Question">
                    
                </div>
                <div class="d-flex justify-content-center">
                    <button class="btn btn-primary btn-large" type="sumbit">Save this assessment</button>
                </div>
            </div>
  
      
    </form>
   
</div>





    <div class="container">
        <div id="assessment-container"></div>
    </div>
    
<script>

    // var areas = @json($areas);
    // var questions = @json($questions);
    var id = {{ $assessment->id }};



</script>


@endsection