<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $data = json_encode(["Comm box", "Sideline", "Pod", "Ringside"]);
        DB::table('areas')->insert([
            'area' => $data,
            'supervisor_id' => 1
        ]);

        $questions = json_encode(["Can Tune Sennheiser", "Can use WSM", "Knows Comm box etiquette"]);

        DB::table('assessments')->insert([
            'supervisor_id' => 1,
            'all_questions_JSON' => $questions,
            'date_created' => now()
    ]);
    }
}
