<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }
  
}
