<?php

namespace App\Http\Controllers;

//use ___PHPSTORM_HELPERS\object;
use Illuminate\Http\Request;

use App\Assessment;

use App\Area;

use App\Question;
use App\User;
use Illuminate\Http\Response;

class AssessmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Assessment $assessment)
    {
        $assess = $assessment->where('supervisor_id', 1)->get();
        

        return view('supervisor.index', compact('assess'));
    }

    public function asyncGet(Assessment $assessment)
    {
  
     // $assessAjax =  $assessment->where('id', $id)->get();
     // return $assessAjax[0]->compiled_assessment;
     return $assessment->compiled_assessment;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Assessment $assessment)
    {
        return view('supervisor.create', compact('assessment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = array_filter($request->area);
        $main = [];
       
        for($i=0; $i<count($area); $i++)
        {
            $question = array_filter($request->question);
          //  $filterQuestion = array_filter($question);
            $array = [
                'area' => $area[$i], 
                'question' => $question
            ];

            $main[] = array_filter($array);
            
        }
        $addArea = $request->addArea;
        $addQuestion = $request->addQuestion;

        if($addArea === null){
            $main ;
            
        } else {
            $inputArray = [
                'area' => $addArea,
                'question' => $addQuestion
            ];
            array_push($main, $inputArray);
        }

        $json = json_encode($main);

        $query = new Assessment();

        $query->job = $request->job;

        $query->compiled_assessment = $json;

        $query->supervisor_id = 1;

        $query->save();

        return redirect('/supervisor');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Assessment $assessment)
    {
        $a = $assessment->compiled_assessment;
        $assess = json_decode($a, true);
   
      
        return view('supervisor.show', compact('assessment', 'assess'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Assessment $assessment, Area $area, Question $question)
    {
        $a = $assessment->compiled_assessment;
        $assess = json_decode($a, true);
   
        $areas = Area::pluck('area');
        $questions = Question::pluck('question');
    
        return view('supervisor.edit', compact('assess', 'assessment', 'questions', 'areas'));
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assessment $assessment)
    {
        $area = array_filter($request->area);
        $main = [];
       
        for($i=0; $i<count($area); $i++)
        {
            $question = array_filter($request->$i);
          //  $filterQuestion = array_filter($question);
            $array = [
                'area' => $area[$i], 
                'question' => $question
            ];

            $main[] = array_filter($array);
            
        }
        $addArea = $request->addArea;
        $addQuestion = $request->addQuestion;

        if($addArea === null){
            $main ;
        } else {
            $inputArray = [
                'area' => $addArea,
                'question' => $addQuestion
            ];
            array_push($main, $inputArray);
        }

        $json = json_encode($main);

        $assessment->update(['job'=>$request->job, 'compiled_assessment' => $json]);


        return back();


    }

    public function asyncUpdate(Request $request, Assessment $assessment)
    {
        $assessment->update(['compiled_assessment' => $request->json]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
