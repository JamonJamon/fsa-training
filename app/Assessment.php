<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $guarded = [];
    //protected $table = 'assessments';\
  
    public function area()
    {
        return $this->hasMany(Area::class);
    }

    public function question()
    {
        return $this->hasMany(Question::class);
    }
}
